# LEMP with Ansible

Chuan bi moi truong:
- Ansible Server: 192.168.10.100/24
- CentOS7-Server2: 192.168.10.101/24
- CentOS7-Server3: 192.168.10.102/24  (***)
- ubuntu20.01: 192.168.10.103/24

####Trien khai: ####
1. Thuc hien tren Ansbile Server
- Cap nhat 
+ yum update -y
- cai dat epel-release
yum install epel-release -y

yum install ansible -y

2. Kiem tra Version cua ansible
ansible --version
ansible 2.9.25
  config file = /etc/ansible/ansible.cfg
  configured module search path = [u'/root/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python2.7/site-packages/ansible
  executable location = /usr/bin/ansible
  python version = 2.7.5 (default, Nov 16 2020, 22:23:17) [GCC 4.8.5 20150623 (Red Hat 4.8.5-44)]
[root@ansible ~]#

3. Cau hinh SSH key va khai bao Inventory
co 2 cach de khai bao de ansible server quan ly cac host
+ c1: su dung user va port cua ssh de khai trong inventory
+ c2: su dung ssh keypair. de tao ra private key va public key
tren ansible server va copy qua cac node (host)

*** Dung tren Ansible Server
dung user root, tao key
ssh-keygen

ssh-copy-id root@192.168.10.101
ssh-copy-id root@192.168.10.102
ssh-copy-id congdd@192.168.10.103

+ khai file inventory
- thuc hien tren ansible Server
/etc/ansible/host
cp /etc/ansible/host /etc/ansible/host.bka
yum install nano -y
nano /etc/ansible/host

192.168.10.110

[centos7]
192.168.10.101
192.168.10.102

[ubuntu]
192.168.10.103

// Liet ke cac host
- ansible all --list-host


// Hien thi 1 group host
ansible [ten-nhom] --list-host


// Sua lai file /etc/ansible/host
client2 ansible_host=192.168.10.101 ansible_port=22 ansible_user=root